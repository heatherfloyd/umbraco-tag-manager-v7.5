﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;

namespace Yoyocms.Umbraco7.TagManager.Models
{
    [TableName("cmsTags")]
    [PrimaryKey("id")]
    [DataContract(Name = "cmsTags", Namespace = "")]
    public class cmsTags
    {
        [DataMember(Name = "id")]
        public int id { get; set; }

        [DataMember(Name = "tag")]
        public string tag { get; set; }

        [DataMember(Name = "group")]
        public string group { get; set; }

        [DataMember(Name = "propertytypeid")]
        public int propertytypeid { get; set; }

        [DataMember(Name = "noTaggedNodes")]
        public int noTaggedNodes { get; set; }

        [DataMember(Name = "tagsInGroup")]
        public TagInGroup tagsInGroup { get; set; }

        [DataMember(Name = "taggedDocuments")]
        public List<TaggedDocument> taggedDocuments { get; set; }

        [DataMember(Name = "taggedMedia")]
        public List<TaggedMedia> taggedMedia { get; set; }


    }
}